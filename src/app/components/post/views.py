from typing import Any, Dict
from jinja2 import Template
from starlette.requests import Request
from starlette_admin import TagsField
from starlette_admin.contrib.sqlmodel import ModelView
from starlette_admin.exceptions import FormValidationError

from app.fields import MarkdownField
from app.components.post.models import Post


class PostView(ModelView):
    fields = [
        "id",
        "title",
        MarkdownField("content"),
        TagsField("tags", render_function_key="tags"),
        "published_at",
        "publisher",
        "comments",
    ]
    exclude_fields_from_list = [Post.content]
    exclude_fields_from_create = [Post.published_at]
    exclude_fields_from_edit = ["published_at"]
    detail_template = "post_detail.html"

    async def validate(self, request: Request, data: Dict[str, Any]) -> None:
        """
        Add custom validation to validate publisher as SQLModel
        doesn't validate relation fields by default
        """
        if data["publisher"] is None:
            raise FormValidationError({"publisher": "Can't add post without publisher"})
        return await super().validate(request, data)

    async def select2_result(self, obj: Any, request: Request) -> str:
        template_str = (
            "<span><strong>Title: </strong>{{obj.title}}, <strong>Publish by: </strong>{{obj.publisher.full_name}}</span>"
        )
        return Template(template_str, autoescape=True).render(obj=obj)

    def can_delete(self, request: Request) -> bool:
        return False

    def can_edit(self, request: Request) -> bool:
        return "admin" in request.state.user["roles"]