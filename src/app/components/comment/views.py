from starlette.requests import Request
from starlette_admin.contrib.sqlmodel import ModelView

from app.components.comment.models import Comment


class CommentView(ModelView):
    page_size = 5
    page_size_options = [5, 10]
    exclude_fields_from_create = ["created_at"]
    exclude_fields_from_edit = ["created_at"]
    searchable_fields = [Comment.content, Comment.created_at]
    sortable_fields = [Comment.pk, Comment.content, Comment.created_at]

    def is_accessible(self, request: Request) -> bool:
        return "admin" in request.state.user["roles"]
