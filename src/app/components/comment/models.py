import enum
from datetime import datetime
from typing import List, Optional, Union

from pydantic import EmailStr
from sqlalchemy import JSON, Column, DateTime, Enum, Text
from sqlalchemy_file import File, ImageField
from sqlalchemy_file.validators import SizeValidator
from sqlmodel import Field, Relationship, SQLModel

from app.helpers import UploadFile
from app.components.user.models import User
from app.components.post.models import Post


class Comment(SQLModel, table=True):
    pk: Optional[int] = Field(primary_key=True)
    content: str = Field(sa_column=Column(Text), min_length=5)
    created_at: Optional[datetime] = Field(
        sa_column=Column(DateTime(timezone=True), default=datetime.utcnow)
    )

    post_id: Optional[int] = Field(foreign_key="post.id")
    post: Post = Relationship(back_populates="comments")

    user_id: Optional[int] = Field(foreign_key="user.id")
    user: User = Relationship(back_populates="comments")
