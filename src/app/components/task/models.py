from datetime import datetime
from typing import List, Optional
from sqlalchemy import JSON, Column, DateTime
from sqlmodel import Field, SQLModel


class Task(SQLModel, table=True):

    id: Optional[int] = Field(primary_key=True)
    title: str = Field(min_length=3)
    tags: List[str] = Field(sa_column=Column(JSON), min_items=1, min_length=3)
    params: List[str] = Field(sa_column=Column(JSON), min_items=1, min_length=3)

    published_at: Optional[datetime] = Field(
        sa_column=Column(DateTime(timezone=True), default=datetime.utcnow)
    )

    # platform_id: Optional[int] = Field(foreign_key="user.id")
    # publisher: User = Relationship(back_populates="posts")
    #
    # # comments: List["Comment"] = Relationship(back_populates="post")