from typing import Any, Dict

from jinja2 import Template
from sqlalchemy import desc, func, select
from sqlalchemy.orm import Session
from starlette.requests import Request
from starlette.responses import Response
from starlette.templating import Jinja2Templates
from starlette_admin import CustomView, EmailField, TagsField
from starlette_admin.contrib.sqlmodel import ModelView
from starlette_admin.exceptions import FormValidationError

from app.fields import MarkdownField
from app.components.user.models import User
from app.components.post.models import Post
from app.components.comment.models import Comment


class UserView(ModelView):
    page_size_options = [5, 10, 25, -1]
    fields = [
        "id",
        "full_name",
        EmailField("username"),
        "avatar",
        "posts",
        "comments",
    ]

    async def select2_result(self, obj: Any, request: Request) -> str:
        url = None
        if obj.avatar is not None:
            storage, file_id = obj.avatar.path.split("/")
            url = request.url_for(
                request.app.state.ROUTE_NAME + ":api:file",
                storage=storage,
                file_id=file_id,
            )
        template_str = (
            '<div class="d-flex align-items-center"><span class="me-2 avatar'
            ' avatar-xs"{% if url %} style="background-image:'
            ' url({{url}});--tblr-avatar-size: 1.5rem;{%endif%}">{% if not url'
            " %}obj.full_name[:2]{%endif%}</span>{{obj.full_name}} <div>"
        )
        return Template(template_str, autoescape=True).render(obj=obj, url=url)

    async def select2_selection(self, obj: Any, request: Request) -> str:
        template_str = "<span>{{obj.full_name}}</span>"
        return Template(template_str, autoescape=True).render(obj=obj)

    async def repr(self, obj: Any, request: Request) -> str:
        return obj.full_name

    def can_delete(self, request: Request) -> bool:
        return False