from sqlalchemy import desc, func, select
from sqlalchemy.orm import Session
from starlette.requests import Request
from starlette.responses import Response
from starlette.templating import Jinja2Templates
from starlette_admin import CustomView
from app.components.user.models import User
from app.components.post.models import Post


class HomeView(CustomView):

    async def render(self, request: Request, templates: Jinja2Templates) -> Response:
        session: Session = request.state.session
        stmt1 = select(Post).limit(10).order_by(desc(Post.published_at))
        stmt2 = (
            select(User, func.count(Post.id).label("cnt"))
            .limit(5)
            .join(Post)
            .group_by(User.id)
            .order_by(desc("cnt"))
        )
        posts = session.execute(stmt1).scalars().all()
        users = session.execute(stmt2).scalars().all()
        return templates.TemplateResponse(
            "home.html", {"request": request, "posts": posts, "users": users}
        )
