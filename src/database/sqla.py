from sqlmodel import create_engine
from settings.config import config

engine = create_engine(config.sqla_engine)
