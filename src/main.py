import uvicorn
from fastapi import FastAPI, APIRouter


from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates

from typing import Optional

from starlette.middleware import Middleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.requests import Request
from starlette_admin import DropDown
from starlette_admin.contrib.sqla import Admin as BaseAdmin
from starlette_admin.views import Link

from settings.config import config
from app.auth import MyAuthProvider

# from app.model import Comment, Post
from app.components.user.models import User
from app.components.post.models import Post
from app.components.task.models import Task
from app.components.comment.models import Comment

from app.components.user.views import UserView
from app.components.task.views import TaskView
from app.components.post.views import PostView
from app.components.comment.views import CommentView

from app.components.home.views import HomeView


from database.sqla import engine




class Admin(BaseAdmin):
    def custom_render_js(self, request: Request) -> Optional[str]:
        return request.url_for("statics", path="js/custom_render.js")


admin = Admin(
    engine,
    title="SQLModel Admin",
    base_url="/admin",
    route_name="admin",
    templates_dir="app/templates/admin",
    logo_url="https://preview.tabler.io/static/logo-white.svg",
    login_logo_url="https://preview.tabler.io/static/logo.svg",
    index_view=HomeView(label="Home", icon="fa fa-home"),
    auth_provider=MyAuthProvider(login_path="/sign-in", logout_path="/sign-out"),
    middlewares=[Middleware(SessionMiddleware, secret_key=config.secret)],
)

admin.add_view(UserView(User, icon="fa fa-users"))
admin.add_view(TaskView(Task, label="Task", icon="fa fa-cogs"))

admin.add_view(PostView(Post, label="Blog Posts", icon="fa fa-blog"))
admin.add_view(CommentView(Comment, icon="fa fa-comments"))
admin.add_view(
    DropDown(
        "Resources",
        icon="fa fa-book",
        views=[
            Link(
                "StarletteAdmin Docs",
                url="https://jowilf.github.io/starlette-admin/",
                target="_blank",
            ),
            Link(
                "SQLAlchemy-file Docs",
                url="https://jowilf.github.io/sqlalchemy-file/",
                target="_blank",
            ),
        ],
    )
)

admin.add_view(Link(label="Go Back to Home", icon="fa fa-link", url="/admin"))

app = FastAPI()

templates = Jinja2Templates(directory="templates")

router = APIRouter()


@router.get("/", tags=["home"])
async def homepage(request):
    return templates.TemplateResponse(
        "index.html", {"request": request, "config": config}
    )

app.mount("/statics", StaticFiles(directory="statics"), name="statics")
admin.mount_to(app)


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8080, reload=True)
