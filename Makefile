start:
	docker-compose up --build -d

stop:
	docker-compose down

lab:
	cd src/lab && jupyter lab
